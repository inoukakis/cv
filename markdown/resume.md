Ioannis Noukakis
============

----

>  Fullstack software engineer with a passion for Test Driven Development.
>  Enthusiastic about embracing new challenges and exploring cutting-edge tools.

----

Experience
----------

**2023 - Now   Fullstack/DevOps engineer at the European Broadcasting Union (EBU):**
Continuation of the Eurovox Project, with multi speaker re-voicing capabilities and voice cloning. Implemented
a unified file translation API across multiple vendors (AWS, Deepl, etc).

**2022 - 2023   Fullstack engineer at Droople:**
At Droople, I joined the team to contribute my technical expertise in various areas such as frontend development,
backend development and Test-Driven Development (TDD). I played a significant role in improving the maintainability
of the codebase. We transitioned from a traditional approach, where manual testing was performed,
to a fully automated regression testing system.

To achieve this, we adopted a Test-Driven Development (TDD) approach and rewrote modules accordingly incrementally.
Additionally, we implemented visual regression testing using Cypress in combination with Percy.
This allowed us to accurately track any visual changes and ensure the stability of our applications.

Furthermore, I collaborated closely with the hardware team to develop automated end-to-end testing solutions.
This involved utilizing the Arduino platform for signal generation and leveraging Python 3 as the primary driver
for this testing suite. This was of vital importance since our end devices had no support for over-the-air updates.

**2022 - 2022   Backend Software Engineer at Migros Online:**

During my time at Migros Online, my responsibilities included maintaining multiple microservices within the service. 
To enhance the reliability of our unit tests, I implemented the concept of utilizing docker containers instead of 
relying on a shared development database.

**2018 - 2022   Fullstack/DevOps engineer at the European Broadcasting Union (EBU):**

During my tenure at the EBU (European Broadcasting Union), I successfully contributed to several impactful projects:

* Realtime Transcription Frontend: Collaborated with France Television to develop a user-friendly tool that displayed real-time transcriptions. Leveraged methodologies such as TTD and Hexagonal Architecture.
* News Pilot Project: Created a B2B user-facing tool that enabled navigation of news articles from various EBU member sources. Implemented automatic article processing and translation using the Eurovox project.
* Eurovox Project - Tool: Developed a user-oriented tool for navigating, transcribing, and translating multilingual content.
* Eurovox Project - API: Built an API that facilitated integration between EBU member production systems and third-party AI vendors. Supported batch and streamed transcription.

\pagebreak

**2017 - 2018   Frontend Engineer – Cofounder at Welitics:**

Welitics was startup project that aims to extend political participation via a social network
dedicated to this topic.

Technical Experience
--------------------

Programming Languages
:   **TypeScript:** Throughout my career, I have gained extensive experience in utilizing the language. 
I have successfully developed frontend applications using technologies like React, Redux, Redux-Toolkit, and Jest.
In addition, I have also built server-side applications using Serverless.js and Express.js.
Moreover, I had the opportunity to delve into basic 3D web development by leveraging Three.js to construct my personal website.

:   **Java 8/11/20/21/Kotlin:** In my professional journey, I have utilized Java initially and later
transitioned to Kotlin to construct multiple REST APIs using the Spring framework. These APIs played a
vital role in a key EBU project in developing an integration solution, serving as a
unified interface for various third-party AI vendors like AWS, Deepl, Azure, Speechmatics, and more.
An additional API served as backend for a collaborative video translation tool with subtitles and voiceovers were
I integrated FFMPEG in the project along with some distributed workflow using RabbitMQ.
Additionally, I have employed Akka Streams to create real-time speech-to-text integrations.

:   **Python 3:** During my tenure at the EBU, I had the opportunity to utilize this language extensively within a DevOps
environment. This involved custom prometheus target finders for our ECS tasks along with other utilities for observability.
At Droople, I further leveraged this language to develop a comprehensive testing suite specifically designed for IoT devices,
interfacing with an Arduino platform using serial communication.
Moreover, I utilized this language to deploy basic APIs using FastAPI for quick prototypes.

:   Basic knowledge of **C++**, **Rust**, **Golang**

DevOps

:   **Terraform:**  We used Terraform at the EBU as our de-facto infrastructure-as-code tool. All projects resources
where provisioned using Terraform in AWS.

:   **Kubernetes:** We used Kubernetes to have replicable on-premise installations and to avoid vendor lock-in.
We settled to use Argo-CD for self-healing.

:   **Prometheus/Grafana:** Our de-facto monitoring tools. Alerts were managed in grafana.

Small Side Projects
:   **Entity Component System in Rust:** I've learned the basics of Rust while implementing a small Entity Component System concept, leveraging Rust's macro
system.\
gitlab.com/inoukakis/old-bell-engine
:   **File downloader in go:** I've build a simple file downloader in go using TDD and hexagonal architecture.\
gitlab.com/inoukakis/old-bell-native-manager
\pagebreak

Education
---------

2021
:   **Introduction to Test Driven Development and Clean Arch in Java**; WealCome - Mooc

    *Session of 10 hours about the process of Test Driven Development and the Clean Architecture (Hexagonal). Students cloud apply those concepts to a small Spring application.*

2013–2017
:   **Bachelor in Computer Science**; HEIG-VD - Yverdon-les-Bains

2012 - 2013
:   **Commercial Professional Maturity**; Gymnasium of Morges

Miscellaneous
----------------------------------------

* Languages:

     * French (native speaker)
     * English (C1)

* Hobbies:

     * D&D 5th edition
     * Sci-Fi/Heroic Fantasy
     * History
     * Video Games

* References:

    * Bastien Rojanawisut - Droople - bastien.rojanawisut@droople.com
    * Ramzi Bouzerda - Droople - ramzi@droople.com
    * Ben Poor - EBU - poor@ebu.ch
    * Sébastien Noir - EBU - noir@ebu.ch
    * Andres Perez-Uribe - HEIG-VD – andres.perez-uribe@heig-vd.ch

----

> <inoukakis@gmail.com> • +41 (0)77 407 64 62 • 30 years old\
> address - Vevey, Switzerland\
> Gitlab: https://gitlab.com/inoukakis\
> https://noukakis.ch