Ioannis Noukakis
============

----

>  Ingénieur logiciel fullstack avec une passion pour le Test Driven Development.
>  Enthousiaste à l'idée de relever de nouveaux défis et d'explorer des outils de pointe.

----

Expérience
----------

**2023 - Now   Fullstack/DevOps engineer à L'union Européenne de Radio-télévision (EBU-UER):**
Poursuite du projet Eurovox, avec des capacités de re-voicing et de clonage de voix pour plusieurs locuteurs. Mise en œuvre
une API de traduction de fichiers unifiée entre plusieurs fournisseurs (AWS, Deepl, etc.).

**2022 - 2023   Fullstack engineer à Droople:**
Chez Droople, j'ai rejoint l'équipe pour apporter mon expertise technique dans différents domaines tels que le développement 
frontend, le développement backend et le développement piloté par les tests (TDD). J'ai joué un rôle important dans 
l'amélioration de la maintenabilité de la base de code. Nous sommes passés d'une approche traditionnelle, où les tests 
étaient effectués manuellement, à un système de tests entièrement automatisé.

Pour ce faire, nous avons adopté une approche de développement piloté par les tests (Test-Driven Development - TDD)
et réécrit les modules de manière incrémentale. En outre, nous avons mis en œuvre des tests de régression visuels 
en utilisant Cypress en combinaison avec Percy. Cela nous a permis de suivre avec précision tout changement visuel et de
garantir la stabilité de nos applications.

En outre, j'ai collaboré étroitement avec l'équipe hardware pour développer des solutions de test end-to-end.
Cela impliquait l'utilisation de la plateforme Arduino pour la génération de signaux et l'utilisation de Python 3
comme principal pilote pour cette suite de tests. Ceci était d'une importance vitale puisque nos produits IoT 
n'avaient pas de support pour les mises à jour over-the-air.

**2022 - 2022   Backend Software Engineer à Migros Online:**

Pendant mon mandat à Migros Online, j'étais chargé de maintenir plusieurs microservices au sein du service.
Pour améliorer la fiabilité de nos tests unitaires, j'ai mis en œuvre le concept d'utiliser de conteneurs 
Docker au lieu de dépendre d'une base de données de développement partagée.

**2018 - 2022   Fullstack/DevOps engineer à L'union Européenne de Radio-télévision (EBU-UER):**

Au cours de mon mandat à l'UER (Union européenne de radio-télévision), j'ai contribué avec succès à plusieurs projets importants :

* Realtime Transcription Frontend: Collaboration avec France Télévision pour développer un outil 
permettant d'afficher des transcriptions en temps réel. Déveplopé selon les bonnes pratiques et méthodologies telles que
le TTD et l'architecture hexagonale.
* News Pilot Project: Création d'un outil B2B, permettant de naviguer dans les articles
d'actualité provenant de diverses sources des membres de l'UER. Mise en œuvre de la traduction automatique des articles
ainsi que de leurs médias à l'aide du projet Eurovox.
* Eurovox Project - Tool: Développement d'un outil orienté vers l'utilisateur pour la navigation, la transcription et la
traduction de contenus multilingues.
* Eurovox Project - API: Création d'une API facilitant l'intégration entre les systèmes de production des membres de l'UER 
et les fournisseurs d'IA tiers. Prise en charge de la transcription par lots et en temps réel.

**2017 - 2018   Frontend Engineer – Cofondateur à Welitics:**

Welitics fut un projet de startup qui visait à inciter à la participation politique par le biais d'un réseau social
dédié à ce sujet.

Expérience technique
--------------------

Langages de programmation
:   **TypeScript:** Tout au long de ma carrière, j'ai acquis une grande expérience dans l'utilisation de ce langage.
J'ai développé avec succès des applications frontend en utilisant des technologies telles que React, Redux, Redux-Toolkit et Jest.
En outre, j'ai également construit des applications server-side en utilisant Serverless.js et Express.js.
De plus, j'ai eu l'occasion de me plonger dans le développement web 3D de base en utilisant Three.js pour construire mon site web personnel.

:   **Java 8/11/20/21/Kotlin:** Dans mon parcours professionnel, j'ai d'abord utilisé Java, puis je suis passé à Kotlin pour 
construire de nombreuses API REST à l'aide du framework Spring. Ces API ont joué un rôle un rôle essentiel dans un projet
clé de l'UER en développant une solution d'intégration, servant d'interface unifiée pour divers fournisseurs
d'IA comme AWS, Deepl, Azure, Speechmatics, etc. Une API supplémentaire a servi de backend pour un outil de
traduction vidéo collaboratif avec des sous-titres et des voix off. J'ai également intégré FFMPEG dans cet dernier projet
ainsi que RabbitMQ afin de pouvoir distribuer la charge de travail lié à l'utilisation de FFMPEG.
Enfin, j'ai utilisé Akka Streams pour créer des intégrations en temps réel de solutions speech-to-text de AWS, Azure et autres.

:   **Python 3:** Au cours de mon mandat à l'UER, j'ai eu l'occasion d'utiliser ce langage dans un environnement DevOps.
Cela impliquait des scripts qui servaient à trouver automatiquement l'IP de nos tâches ECS afin de les exposer
à Promeheus ainsi que d'autres utilitaires pour l'observabilité. Chez Droople, j'ai continué à utiliser ce langage pour
développer une suite de tests complète spécialement conçue pour les appareils IoT, interfaçant avec une plateforme Arduino
via un port serial. De plus, j'ai utilisé ce langage pour déployer des REST APIs basiques grâce à FastAPI
pour des prototypes rapides.

:   Connaissances de base en **C++**, **Rust**

DevOps

:   **Terraform:**  À l'UER, nous avons utilisé Terraform comme outil de facto pour l'infrastructure en tant que code.
Toutes les ressources des projets ont été approvisionnées à l'aide de Terraform dans AWS.

:   **Kubernetes:** Nous avons utilisé Kubernetes pour avoir des installations sur site réplicables et pour éviter le 
vendor lock-in. Nous avons décidé d'utiliser Argo-CD pour ses capacités d'autoréparation.

:   **Prometheus/Grafana:** Nos outils de surveillance de facto. Les alertes étaient gérées dans grafana.

Petits projets annexes
:   **Entity Component System en Rust:** J'ai appris les bases de Rust en implémentant un petit concept de système de composants d'entités, en tirant parti 
du système de macro de Rust.\
gitlab.com/inoukakis/old-bell-engine
:   **Téléchargeur de fichiers en go:** J'ai construit un simple téléchargeur de fichiers en go en utilisant le TDD et l'architecture hexagonale.\
gitlab.com/inoukakis/old-bell-native-manager


Education
---------

2021
:   **Formation TDD et Clean Architecture dans le monde Java**; WealCome - Mooc

    *Session de 10 heures sur le processus de développement piloté par les tests (TDD) et l'architecture hexagonale. Les étudiants purent appliquer ces concepts à une petite application Spring.*

2013–2017
:   **Bachelor in Computer Science**; HEIG-VD - Yverdon-les-Bains

2012 - 2013
:   **Maturité professionnelle commerciale**; Gymnase de Morges

Divers
----------------------------------------

* Langues:

     * Français (langue maternelle)
     * Anglais (C1)

* Loisirs:

     * D&D 5ème édition
     * Sci-Fi/Heroic Fantasy
     * Histoire
     * Jeux vidéos

* Références:

    * Bastien Rojanawisut - Droople - bastien.rojanawisut@droople.com
    * Ramzi Bouzerda - Droople - ramzi@droople.com
    * Ben Poor - EBU - poor@ebu.ch
    * Sébastien Noir - EBU - noir@ebu.ch
    * Andres Perez-Uribe - HEIG-VD – andres.perez-uribe@heig-vd.ch

----

> <inoukakis@gmail.com> • +41 (0)77 407 64 62 • 29 ans\
> adress - Vevey, Switzerland\
> Gitlab: https://gitlab.com/inoukakis\
> GitHub: https://github.com/ioannisNoukakis\
> https://noukakis.ch